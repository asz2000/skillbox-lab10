import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
             os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_os_release(host):
    assert host.file("/etc/os-release").contains("CentOS")


def test_nginx_inactive(host):
    assert host.service("nginx").is_running is False


def test_nginx_version(host):
    assert host.package("nginx").version.startswith("1.20.1")
